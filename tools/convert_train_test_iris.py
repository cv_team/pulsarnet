import pandas
from sklearn import model_selection
from sklearn.preprocessing import LabelEncoder
data = pandas.read_csv('../data/iris.csv')
dataset = data.values
print(data.head())
print(dataset)
# split data into X and y
X = dataset[:,0:4]
Y = dataset[:,4]

label_encoder = LabelEncoder()
label_encoder = label_encoder.fit(Y)
label_encoded_y = label_encoder.transform(Y)
seed = 7
test_size = 0.33
X_train, X_test, y_train, y_test = model_selection.train_test_split(X, label_encoded_y, test_size=test_size, random_state=seed)
print(X_train)