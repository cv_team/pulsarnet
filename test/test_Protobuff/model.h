//
// Created by Pulsar on 2019/4/14.
//

#ifndef LEARNDEEP_MODEL_H
#define LEARNDEEP_MODEL_H

#include <Eigen/Dense>

class AlexNet {
public:
    AlexNet(int learning_rate,int input_nodes,int hidden_nodes,int output_nodes);
    int predict(Eigen::MatrixXd input);
    int train(Eigen::MatrixXd inputs_list,Eigen::MatrixXd targets_list);
    ~AlexNet();
private:
    double learning_rate;
    int input_nodes = 0;
    int hidden_nodes = 0;
    int output_nodes = 0;
    Eigen::MatrixXd weights_input_to_hidden;
    Eigen::MatrixXd weights_hidden_to_output ;
    Eigen::MatrixXd baises_input_to_hidden  ;
    Eigen::MatrixXd baises_hidden_to_output  ;

    Eigen::MatrixXd (*activation_function)(Eigen::MatrixXd);
    Eigen::MatrixXd backword(Eigen::MatrixXd final_outputs,Eigen::MatrixXd targets);
    Eigen::MatrixXd forword(Eigen::MatrixXd inputs);
};


#endif //LEARNDEEP_MODEL_H
