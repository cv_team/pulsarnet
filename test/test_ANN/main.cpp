//
// Created by Pulsar on 2019/4/14.
//

#include "main.h"
#include <Eigen/Dense>
//#include <layers/ANNLayer.h>
#include "model.h"

using namespace Eigen;

int main(int argc, char **argv) {
    double learning_rate = 0.001;
    int input_nodes = 2;
    int hidden_nodes = 2;
    int output_nodes = 2;
//    AnnLayer model(learning_rate, input_nodes, hidden_nodes, output_nodes);
    BPNetwork model(learning_rate, input_nodes, hidden_nodes, output_nodes);
    MatrixXd label(4, 2);
    label << 1, 0,
            0, 1,
            0, 1,
            1, 0;
    MatrixXd train_data(4, 2);
    train_data << 0, 0,
            0, 1,
            1, 1,
            0, 0;

    std::cout << "train_data=\n" << train_data << "\n" << std::endl;
    std::cout << "labels=\n" << label << "\n" << std::endl;
    model.train(train_data, label, 10000);
    double acc=0.0;
    double pre_true=0;
    double pre_error=0;
    for (int i = 0; i < train_data.rows(); i++) {
//        Eigen::MatrixXd output = model.query(train_data.row(i));
        Eigen::MatrixXd output = model.predict(train_data.row(i));
        double max = 0.0;
        double label = 0;
        for (int j = 0; j < output.rows(); j++) {
            double temp = (double) output(j, 0);
            if (temp > max) {
                max = temp;
                label = j;
            }
        }
        if((int)label_list(i,0)==label)
            pre_true+=1;
        else
            pre_error+=1;
        std::cout << "\n test_data : " << train_data.row(i) << std::endl;
        std::cout << " max : " << max << " label: " << label << std::endl;
        std::cout << " output : " << output.transpose() << std::endl;
    }



    std::cout<<"ACC: "<<(pre_true/train_data.rows())<<std::endl;

    getchar();

}
