//
// Created by Pulsar on 2019/4/13.
//
#include <iostream>
#include <Eigen/Dense>
#include <activate_functions/sigmod.h>

using namespace std;
using namespace Eigen;
int main(int argc, char **argv) {
    MatrixXd m = MatrixXd::Random(3,3);
    cout << "m =" << endl << m << endl;
    cout << "sigmod(m)=" << endl << (1/(1+1/m.array().exp())) << endl;
    cout << "my_sigmod(m)=\n" << sigmod<MatrixXd>(m)     << endl;
    cout << "Here is m.*m      :      \n " << m.array()*m.array()           << endl;
    cout << "Here is m*m      :       \n" << m*m*1000         << endl;
    cout << "Here is mat.sum():       " << m.sum()       << endl;
    cout << "Here is mat.prod():      " << m.prod()      << endl;
    cout << "Here is mat.mean():      " << m.mean()      << endl;
    cout << "Here is mat.minCoeff():  " << m.minCoeff()  << endl;
    cout << "Here is mat.maxCoeff():  " << m.maxCoeff()  << endl;
    cout << "Here is mat.trace():     " << m.trace()     << endl;
    return 0;
}

