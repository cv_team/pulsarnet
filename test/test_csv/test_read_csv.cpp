//
// Created by Pulsar on 2019/4/18.
//
#include <Eigen/Dense>

#include <opencv2/opencv.hpp>
#include <opencv2/opencv_modules.hpp>
#include <opencv2/core/eigen.hpp>
#include <opencv2/ml/ml.hpp>
#include <ios>
#include <iostream>
#include <string>

using namespace cv;
using namespace cv::dnn;
using namespace cv::ml;
using namespace std;
using namespace Eigen;

void DeleteOneColOfMat(Mat &object, int num) {
    if (num < 0 || num >= object.cols) {
        cout << "  " << endl;
    } else {
        if (num == object.cols - 1) {
            object = object.t();
            object.pop_back();
            object = object.t();
        } else {
            for (int i = num + 1; i < object.cols; i++) {
                object.col(i - 1) = object.col(i) + Scalar(0, 0, 0, 0);
            }
            object = object.t();
            object.pop_back();
            object = object.t();
        }
    }
}

int main(int argc, char **argv) {
    Ptr<ml::TrainData> train_data;
//    for(int x=0;x<100;x+=1){
    train_data = ml::TrainData::loadFromCSV("E:\\WorkSpace\\TestCode\\LearnDeep\\data\\mnist\\mnist_test.csv", 1);

    Mat t_data = train_data->getTrainSamples();
    Mat label=t_data.col(0);
//    DeleteOneColOfMat(t_data,0);
        UMat umat;
    t_data.copyTo(umat);
    cout << t_data.rows << "," << t_data.cols << endl;
    for (int i = 0; i < 1; i += 1) {
        Mat data = t_data.row(i).clone();
        data = data.reshape(1, 28);
        data.ptr<uchar>(0)[1] = 0;
        data.ptr<uchar>(0)[1] = 0;
        data.ptr<uchar>(0)[2] = 0;
        label.convertTo(label, CV_8UC1);
        int val=(int)label.at<uchar>(i,0);
        int row = data.rows;
        int col = data.cols;
        MatrixXd e_mat(row, col);
        cv2eigen(data,e_mat);
        cout <<"e_mat=\n"<< e_mat << endl;
        cout << data.rows << "," << data.cols << "," << data.channels() <<" Label: "<<val<< endl;
        imshow("CSV", data);
        cv::waitKey(-1);
    }
    return 0;
}

