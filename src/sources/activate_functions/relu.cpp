//
// Created by Pulsar on 2019/4/13.
//

#include <activate_functions/relu.h>
#include "relu.lnf"
using namespace std;
template <class T>
T relu(T matrix){
    return (1/(1+1/matrix.array().exp()));
}