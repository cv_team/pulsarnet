//
// Created by Pulsar on 2019/4/14.
//

#include <activate_functions/sigmod.h>
#include "sigmod.lnf"

using namespace std;
template <class T>
T sigmod(T matrix){
    return (1/(1+1/matrix.array().exp()));
}
#ifdef CUDA

#elif OPENCL

#endif
