//
// Created by Pulsar on 2019/4/13.
//

#include <layers/Conv2dLayer.h>

Conv2dLayer::Conv2dLayer(double _learning_rate, int _input_nodes, int _hidden_nodes, int _output_nodes) {

}

Conv2dLayer::~Conv2dLayer() {

}

double Conv2dLayer::backword(Eigen::MatrixXd final_outputs, Eigen::MatrixXd hidden_outputs, Eigen::MatrixXd inputs,
                             Eigen::MatrixXd targets) {
    return 0;
}

Eigen::MatrixXd Conv2dLayer::query(Eigen::MatrixXd input) {
    return Eigen::MatrixXd();
}

int Conv2dLayer::train(Eigen::MatrixXd train_data, Eigen::MatrixXd label, int epoch) {
    return 0;
}

int Conv2dLayer::train(Eigen::MatrixXd train_data_line, Eigen::MatrixXd label) {
    return 0;
}

std::vector<Eigen::MatrixXd> Conv2dLayer::forword(Eigen::MatrixXd inputs) {
    return std::vector<Eigen::MatrixXd>();
}
