//
// Created by Pulsar on 2019/4/13.
//

#include <DataParser.h>

Eigen::MatrixXd DataParser::uMat2EigenMatrix(cv::UMat cv_matrix) {
    return Eigen::MatrixXd();
}

cv::UMat DataParser::eigenMatrix2UMat(Eigen::MatrixXd eigen_matrix) {
    return cv::UMat();
}

Eigen::MatrixXd DataParser::mat2EigenMatrix(cv::Mat cv_matrix) {
    return Eigen::MatrixXd();
}

cv::Mat DataParser::eigenMatrix2Mat(Eigen::MatrixXd eigen_matrix) {
    return cv::Mat();
}

Eigen::MatrixXd DataParser::load_csv(std::string cv_path) {
    return Eigen::MatrixXd();
}

Eigen::MatrixXd DataParser::load_txt(std::string txt_path) {
    return Eigen::MatrixXd();
}

Eigen::MatrixXd DataParser::load_lmdb(std::string database_path) {
    return Eigen::MatrixXd();
}

std::vector<std::string> DataParser::load_image_from_list(std::string list_file_path) {
    return std::vector<std::string>();
}

cv::Mat DataParser::load_image(std::string list_file_path) {
    return cv::Mat();
}
