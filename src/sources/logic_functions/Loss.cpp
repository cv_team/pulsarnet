//
// Created by Pulsar on 2019/4/13.
//
#include <logic_functions/Loss.h>
#include "Loss.lnf"
template <typename T>
T loss_variance(T target, T final_output){
    return (target-final_output).array()*(target-final_output).array();
}