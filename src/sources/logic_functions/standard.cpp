//
// Created by Pulsar on 2019/4/15.
//

#include <logic_functions/standard.h>
template <typename T>
T standard(T X){
    T X_mean = X.mean();
    T X_std = (((X - X_mean) ** 2).sum() / len(X)) ** 0.5
    return (X - X_mean) / X_std
}
