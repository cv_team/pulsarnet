//
// Created by Pulsar on 2019/4/15.
//

#include <logic_functions/MaxMin.h>
#include "MaxMin.lnf"
///
/// \tparam T Data Type
/// \param X Input Data
/// \return Normalization Data
template <typename T>
T normalization(T X){
    //(X - X_min) / (X_max - X_min)
    Eigen::MatrixXd X_min = X.minCoeff();
    Eigen::MatrixXd X_max = X.minCoeff();
    Eigen::MatrixXd exp_1=(X - X_min);
    Eigen::MatrixXd exp_2=(X_max - X_min);
    return exp_1/exp_2;
}