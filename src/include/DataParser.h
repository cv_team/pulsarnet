//
// Created by Pulsar on 2019/4/13.
//

#ifndef PROJECT_DATAPARSER_H
#define PROJECT_DATAPARSER_H

#include <Eigen/Dense>
#include <opencv2/opencv.hpp>
#include <string>
#include <vector>

class DataParser {
    static Eigen::MatrixXd uMat2EigenMatrix(cv::UMat cv_matrix);

    static cv::UMat eigenMatrix2UMat(Eigen::MatrixXd eigen_matrix);

    static Eigen::MatrixXd mat2EigenMatrix(cv::Mat cv_matrix);
    
    static cv::Mat eigenMatrix2Mat(Eigen::MatrixXd eigen_matrix);

    static Eigen::MatrixXd load_csv(std::string cv_path);

    static Eigen::MatrixXd load_txt(std::string txt_path);

    static Eigen::MatrixXd load_lmdb(std::string database_path);

    static std::vector<std::string> load_image_from_list(std::string list_file_path);

    static cv::Mat load_image(std::string image_path);
};


#endif //PROJECT_DATAPARSER_H
