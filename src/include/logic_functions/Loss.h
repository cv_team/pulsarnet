//
// Created by Pulsar on 2019/4/13.
//

#ifndef LEARNDEEP_LOSS_H
#define LEARNDEEP_LOSS_H


template <class T>
T loss_variance(T targets,T final_outputs);
void _loss_variance();

#endif //LEARNDEEP_LOSS_H
