//
// Created by Pulsar on 2019/4/13.
//

#ifndef LEARNDEEP_CONV2DLAYER_H
#define LEARNDEEP_CONV2DLAYER_H

#include <Eigen/Dense>
#include <vector>

class Conv2dLayer {
public:
    Eigen::MatrixXd (*activation_function)(Eigen::MatrixXd);

    Eigen::MatrixXd (*loss_function)(Eigen::MatrixXd, Eigen::MatrixXd);

    Conv2dLayer(double _learning_rate, int _input_nodes, int _hidden_nodes, int _output_nodes);

    Eigen::MatrixXd query(Eigen::MatrixXd input);

    int train(Eigen::MatrixXd train_data, Eigen::MatrixXd label, int epoch);

    int train(Eigen::MatrixXd train_data_line, Eigen::MatrixXd label);

    ~Conv2dLayer();

private:
    double learning_rate;
    int input_nodes;
    int hidden_nodes;
    int output_nodes;
    Eigen::MatrixXd weights_input_to_hidden;
    Eigen::MatrixXd weights_hidden_to_output;
    Eigen::MatrixXd baises_input_to_hidden;
    Eigen::MatrixXd baises_hidden_to_output;

    double backword(Eigen::MatrixXd final_outputs, Eigen::MatrixXd hidden_outputs, Eigen::MatrixXd inputs,
                    Eigen::MatrixXd targets);

    std::vector<Eigen::MatrixXd> forword(Eigen::MatrixXd inputs);
};


#endif //LEARNDEEP_CONV2DLAYER_H
