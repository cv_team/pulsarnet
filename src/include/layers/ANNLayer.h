//
// Created by Pulsar on 2019/4/15.
//

#ifndef LEARNDEEP_ANNLAYER_H
#define LEARNDEEP_ANNLAYER_H

#include <Eigen/Core>
#include <vector>

class AnnLayer {
public:
    Eigen::MatrixXd (*activation_function)(Eigen::MatrixXd);

    Eigen::MatrixXd (*loss_function)(Eigen::MatrixXd, Eigen::MatrixXd);

    AnnLayer(double _learning_rate, int _input_nodes, int _hidden_nodes, int _output_nodes);

    Eigen::MatrixXd query(Eigen::MatrixXd input);

    int train(Eigen::MatrixXd train_data, Eigen::MatrixXd label, int epoch);

    int train(Eigen::MatrixXd train_data_line, Eigen::MatrixXd label);

    ~AnnLayer();

private:
    double learning_rate;
    int input_nodes;
    int hidden_nodes;
    int output_nodes;
    Eigen::MatrixXd weights_input_to_hidden;
    Eigen::MatrixXd weights_hidden_to_output;
    Eigen::MatrixXd baises_input_to_hidden;
    Eigen::MatrixXd baises_hidden_to_output;

    double backword(Eigen::MatrixXd final_outputs, Eigen::MatrixXd hidden_outputs, Eigen::MatrixXd inputs,
                    Eigen::MatrixXd targets);

    std::vector<Eigen::MatrixXd> forword(Eigen::MatrixXd inputs);
};

#endif //LEARNDEEP_ANNLAYER_H
