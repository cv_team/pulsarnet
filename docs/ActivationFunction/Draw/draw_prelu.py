# -*- coding:utf-8 -*-
from matplotlib import pyplot as plt
import numpy as np
import mpl_toolkits.axisartist as axisartist


def prelu(x):
    return np.where(x<0,0.5*x,x)


def plot_prelu():
    x = np.arange(-10, 10, 0.1)
    y = prelu(x)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.spines['top'].set_color('none')
    ax.spines['right'].set_color('none')
    # ax.spines['bottom'].set_color('none')
    # ax.spines['left'].set_color('none')
    ax.spines['left'].set_position(('data', 0))
    ax.spines['bottom'].set_position(('data', 0))
    ax.plot(x, y)
    plt.xticks([])
    plt.yticks([])
    plt.tight_layout()
    plt.savefig("prelu.png")
    plt.show()


if __name__ == "__main__":
    plot_prelu()
