# -*- coding:utf-8 -*-
from matplotlib import pyplot as plt
import numpy as np
import mpl_toolkits.axisartist as axisartist


def relu(x):
    return np.where(x<0,0,x)

def plot_relu():
    x = np.arange(-10, 10, 0.1)
    y = relu(x)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.spines['top'].set_color('none')
    ax.spines['right'].set_color('none')
    # ax.spines['bottom'].set_color('none')
    # ax.spines['left'].set_color('none')
    ax.spines['left'].set_position(('data', 0))
    ax.plot(x, y)
    plt.xlim([-10.05, 10.05])
    plt.ylim([0, 10.02])
    ax.set_yticks([2, 4, 6, 8, 10])
    plt.tight_layout()
    plt.savefig("relu.png")
    plt.show()



if __name__ == "__main__":
    plot_relu()
